package camelcase;

import java.util.Scanner;

public class CamelCase {

    public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);
       translate("zadejte retezec");
       while(true){
           String a = sc.nextLine();
           translate(a);
       }
    }
    public static void translate(String input){
        StringBuilder out = new StringBuilder();
        if (input.isBlank()) 
            return;
        while(input.contains(" ")){
            String b;
            String c;
            c = input.substring(0,1).toUpperCase();
            b = input.substring(1,input.indexOf(" "));            
            out.append(c);
            out.append(b);
            input = input.substring(input.indexOf(" ")+1);
        }
        while(input.contains("-")){
            String b;
            String c;
            c = input.substring(0,1).toUpperCase();
            b = input.substring(1,input.indexOf("-"));            
            out.append(c);
            out.append(b);
            input = input.substring(input.indexOf("-")+1);
        }
        while(input.contains("_")){
            String b;
            String c;
            c = input.substring(0,1).toUpperCase();
            b = input.substring(1,input.indexOf("_"));            
            out.append(c);
            out.append(b);
            input = input.substring(input.indexOf("_")+1);
        }
        if(input.length()>=1){
            String c;
            c = input.substring(0,1).toUpperCase();
            out.append(c);
            out.append(input.substring(1));
        }
        System.out.println(out);       
    }
}
